FROM google/cloud-sdk:slim
RUN apt-get update -y && \
    apt-get install -y make && \
    apt-get clean
ADD requirements*.txt /
RUN pip install -r /requirements-dev.txt && \
    rm /requirements*.txt